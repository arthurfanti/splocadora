## Sâo Paulo Locadora

Installing:
------------

1.
```
git clone git@bitbucket.org:domini/sao-paulo-locadora.git
```

2.
```
npm install
```

3.
```
bower install
```

4.
```
grunt bower-install
```

Running:
---------

1.
```
grunt
```

Deploy:
--------

1.
```
grunt publish
```