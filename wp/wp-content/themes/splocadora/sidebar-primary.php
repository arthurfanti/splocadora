<aside role="sideBar">
	<fieldset>
		<legend>veja mais</legend>
		<ul class="small-block-grid-1">
			<li>
				<a href="<?= get_post_type_archive_link('produtos'); ?>" class="button expand text-left icon-tractor">confira todos nossos produtos</a>
			</li>
			<!-- <li>
				<a href="<?= get_permalink( get_page_by_title('filiais') ); ?>" class="button expand text-left icon-home">encontre a filial mais próxima</a>
			</li> -->
			<li>
				<a href="#" class="button expand text-left icon-doc">guia completo da construção civil</a>
			</li>
			<li>
				<a href="#" class="button expand text-left icon-cone">quais os passos para alugar equipamento?</a>
			</li>
		</ul>
	</fieldset>
	<?php if (!is_page('contato')): ?>
	<fieldset>
		<legend>veja também</legend>
		<ul class="small-block-grid-1">
			<?php $products_query = new WP_Query(array('post_type' => 'produtos', 'posts_per_page' => 3, 'orderby' => 'rand')); ?>
			<?php if ( $products_query->have_posts() ) : while ( $products_query->have_posts() ) : $products_query->the_post(); ?>
			<?php
				if (has_post_thumbnail()) {
					$_prod_thumbs = wp_get_attachment_image_src(get_post_thumbnail_id(), array(36,36));
					$_prod_thumb  = $_prod_thumbs[0];
				} else {
					$_prod_thumb = "";
				}
			?>
			<!-- post -->
			<li>
				<div class="text-center">
					<?= the_post_thumbnail( array(240,240) ); ?>
					<h4 class="primary"><?= the_title(); ?></h4>
					<a href="<?= the_permalink(); ?>" class="button uppercase tiny radius">ver mais</a>
					<a href="#" class="button button-icon secondary uppercase tiny radius side-add-to-cart-button" data-prod-info='{"id":"<?php the_ID(); ?>","name":"<?php the_title(); ?>","link":"<?php the_permalink() ?>","thumbUrl":"<?php echo $_prod_thumb; ?>"}'>adicionar&nbsp;<i class="icon-cart"></i>
					</a>
				</div>
			</li>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
			<!-- post navigation -->
			<?php else: ?>
			<h4>:(</h4>
			<?php endif; ?>
		</ul>
	</fieldset>
	<?php endif ?>
	<!-- <div class="twitterWrapper icon-twitter">
		Focco Locadora sempre trazendo novidades e tecnologia de ponta para a Construção Civil <a href="#">pic.tiwitter.com/SPLOCADORA</a>
	</div> -->
</aside>