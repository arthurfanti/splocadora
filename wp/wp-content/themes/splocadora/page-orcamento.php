<?php
	$products_query = new WP_Query(array('post_type' => 'produtos', 'posts_per_page' => -1 ));

	if ($products_query->have_posts()) {
		while ($products_query->have_posts()) {
			$products_query->the_post();

			$thumbUrl = (has_post_thumbnail()) ? wp_get_attachment_image_src( get_post_thumbnail_id(), array(36,36) ) : "" ;
			$thumbUrl = $thumbUrl[0];
			
			$products_array[] = array(
				'id'       => get_the_id(),
				'name'     => get_the_title(),
				'link'     => get_the_permalink(),
				'thumbUrl' => $thumbUrl,
			);
		}
	}

	$products_json = json_encode($products_array);
	get_header('internas');
?>
			<div class="row">
				<div class="small-12 columns page-header">
					<h1>
						<!--<?= get_the_title(); ?>-->Orçamento rápido
					</h1>
					<p class="lead">Alugar máquinas é nosso trabalho</p>
				</div>
				<div class="small-8 columns">
					<article role="budgetContent">
						<form name="senddata" method="POST">
							<fieldset>
								<legend>Dados de contato</legend>
								<div class="row">
									<div class="small-12 columns">
										<label for="name">Empresa:</label>
										<input type="text" id="name" name="corp" placeholder="Empresa" tabindex="1">
									</div>
									<div class="small-12 columns">
										<label for="name">Nome para contato:</label>
										<input type="text" id="name" name="name" placeholder="Nome" tabindex="2">
									</div>
									<div class="small-6 columns">
										<label for="email">Email:</label>
										<input type="text" id="email" name="email" placeholder="Email" tabindex="3">
									</div>
									<div class="small-6 columns">
										<label for="cnpj">CNPJ:</label>
										<input type="text" id="cnpj" name="cnpj" placeholder="CNPJ" tabindex="4">
									</div>
									<div class="small-2 columns">
										<label for="ddd">DDD:</label>
										<input type="text" id="ddd" name="ddd" placeholder="DDD" maxlength="2" tabindex="5">
									</div>
									<div class="small-10 columns">
										<label for="phone">Telefone:</label>
										<input type="text" id="phone" name="phone" placeholder="Telefone" tabindex="6">
									</div>
									<div class="small-10 columns">
										<label for="address">Endereço:</label>
										<input type="text" id="address" name="address" placeholder="Endereço" tabindex="7">
									</div>
									<div class="small-2 columns">
										<label for="address_number">Número:</label>
										<input type="text" id="address_number" name="address_number" placeholder="Numero" maxlength="5" tabindex="8">
									</div>
									<div class="small-12 columns">
										<label for="obs">Observações:</label>
										<textarea name="obs" id="obs" cols="30" rows="8" tabindex="9"></textarea>
									</div>
								</div>
							</fieldset>
							<fieldset>
								<legend>Produtos</legend>
								<div class="row collapse">
									<div id="orcamento" class="small-12 columns">
										<div id="produtos"></div>
										<div id="full-product-list">
											<script>window.fullProductList = <?= $products_json; ?></script>
											<div id="products-field" class="row collapse">
												<div class="autocomplete small-10 columns">
													<input type="text" value="" placeholder="Orçar mais produtos" class="autocomplete-input">
												</div>
												<div class="small-1 columns">
													<a href="#" class="button icon-search tiny secondary autocomplete-toggle postfix" title="Mostrar Lista"></a>
												</div>
												<div class="small-1 columns">
													<a href="#" class="button icon-cart tiny secondary add-to-cart-button postfix" title="Adicionar ao Orçamento"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset>
								<legend>Informações sobre a Locação</legend>
								<div class="row collapse" id="locationPeriod">
									<div class="small-1 columns">
										<label class="right inline" for="from_date">De:&nbsp;</label>
									</div>
									<div class="small-10 medium-4 columns">
										<input type="date" id="from_date" name="from_date" tabindex="10">
									</div>
									<div class="small-1 columns">
										<label for="from_date" class="button icon-calendar tiny secondary postfix" title="exibix" calendario=""></label>
									</div>

									<div class="small-1 columns">
										<label class="right inline" for="to_date">Até:&nbsp;</label>
									</div>
									<div class="small-10 medium-4 columns">
										<input type="date" id="to_date" name="to_date" tabindex="11">
									</div>
									<div class="small-1 end columns">
										<label for="to_date" class="button icon-calendar tiny secondary postfix" title="exibix" calendario=""></label>
									</div>
								</div>
							</fieldset>
							<button type="submit" class="tiny radius secondary uppercase">enviar</button>
						</form>
					</article>
				</div>
				<div class="small-4 columns">
					<?php get_sidebar('primary'); ?>
				</div>
			</div>
			

			<?php get_template_part('partials/loop', 'related_news'); ?>

			<?php get_template_part('partials/content', 'subscribe'); ?>
			<script>
				if(Modernizr.inputtypes.date == false){
					$.getScript('js/foundation-datepicker.js', function(){
						var nowTemp = new Date();
						var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
					 
						var checkin = $('#to_date').fdatepicker({
							onRender: function (date) {
								return date.valueOf() < now.valueOf() ? 'disabled' : '';
							}
						}).on('changeDate', function (ev) {
							if (ev.date.valueOf() > checkout.date.valueOf()) {
								var newDate = new Date(ev.date)
								newDate.setDate(newDate.getDate() + 1);
								checkout.update(newDate);
							}
							checkin.hide();
							$('#from_date')[0].focus();
						}).data('datepicker');
						var checkout = $('#from_date').fdatepicker({
							onRender: function (date) {
								return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
							}
						}).on('changeDate', function (ev) {
							checkout.hide();
						}).data('datepicker');
					});
				}
			</script>
<?php get_footer(); ?>