<!doctype html>
<!--[if IE 6]>
<html class-"no-js" id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html class-"no-js" id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class-"no-js" id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html class-"no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php
			// Print the <title> tag based on what is being viewed.
			global $page, $paged;

			wp_title( '|', true, 'right' );

			// Add the blog name.
			bloginfo( 'name' );

			// Add the blog description for the home/front page.
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
				echo " &middot; $site_description";

			// Add a page number if necessary:
			if ( $paged >= 2 || $page >= 2 )
				echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );
		?></title>

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<?php wp_head(); ?>
		<script>
			// URL do template para .js
			<?php
				$wpTemplateUrl = get_template_directory_uri();
				$pattern = '/\/novosite/';

				if ( preg_match($pattern, $wpTemplateUrl) ) {
					$wpTemplateUrl = str_replace($pattern, '', $wpTemplateUrl);
					echo 'window.wpTemplateUrl = "' . $wpTemplateUrl . '";';
				} else {
					echo 'window.wpTemplateUrl = "' . $wpTemplateUrl . '";';
				}
			?>
			// window.wpTemplateUrl = "<?= get_template_directory_uri(); ?>";
		</script>
	</head>
	<body>
		<div data-alert class="alert-box success fixed" style="display:none;">Cadastro realizado com sucesso!<a href="#" class="close">&times;</a></div>
		<div data-alert class="alert-box warn fixed" style="display:none;">Desculpe, algo deu errado :( Tente novamente mais tarde!<a href="#" class="close">&times;</a></div>
		<header>
			<div class="topbar show-for-medium-up">
				<div class="row collapse">
					<div class="small-12 column">
						<ul class="inline-list">
							<li>
								<a href="#">guia completo da construção civil</a>
							</li>
							<li>
								<a href="#">dúvidas frequentes</a>
							</li>
							<li>
								<a href="#">locação de máquinas e equipamentos</a>
							</li>
							<li class="right">
								telefone: <span>0800 770-8300</span>
							</li>
							<li class="right">
								email: <span><a href="mailto:locacao@foccolocadora.com.br" style="color:#F47C20; font-size:13px;">locacao@foccolocadora.com.br</a></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="mainbar internas">
				<div class="row collapse">
					<div class="small-3 small-centered medium-uncentered columns">
						<a href="<?= get_permalink( get_page_by_title('home') ); ?>"><b class="logo">Focco Locadora</b></a>
					</div>
					<div class="navbar medium-9 columns show-for-medium-up">
						<div class="row collapse">
							<div class="small-12 column">
								<nav>
									<ul>
										<li><a id="menuProdutos" href="#">Produtos</a></li>
										<li><a href="http://www.foccolocadora.com.br/focco-locadora-de-maquinas-e-equipamentos/">Focco Locadora</a></li>
										<li><a href="<?= get_post_type_archive_link('noticias'); ?>">Notícias</a></li>
										<li><a href="<?= get_permalink( get_page_by_title('orcamento') ); ?>">Orçamento Rápido</a></li>
										<li><a href="<?= get_permalink( get_page_by_title('contato') ); ?>">Contato</a></li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php get_template_part('partials/content', 'productnav'); ?>
		</header>

		<?php get_template_part('partials/content', 'fixednav'); ?>

		<section role="breadcrumb">
			<div class="row collapse">
				<div class="small-12 column">
					<!-- <ul class="sub-nav">
						<li><a href="#">Home</a></li>
						<li><a href="#">Focco Locadora</a></li>
						<li><a href="#">Produtos</a></li>
						<li><a href="#">Mini grua</a></li>
					</ul> -->
					<?php wp_custom_breadcrumbs(); ?>
				</div>
			</div>
		</section>
		<div id="content" role="main">