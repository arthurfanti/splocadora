<?php
/**
 * Template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header('internas'); ?>

			<div class="row">
			<?php if ( have_posts() ) : ?>
				<div class="small-12 columns page-header">
					<h1><?php printf( __( 'Resultados para: %s', 'twentyeleven' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				</div>

				<div class="small-8 columns">
					<?php while ( have_posts() ) : the_post(); ?>
					<div>
						<a href="<?php the_permalink(); ?>">
							<?php the_title( '<h3>', '</h3>', true ); ?>
							<span><?php the_excerpt(); ?></span>
						</a>
					</div>
					<hr>
					<?php endwhile; ?>
				</div>

			<?php else : ?>

				<div class="small-8 columns">
					<article id="post-0" class="post no-results not-found">
						<header class="entry-header">
							<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
						</header><!-- .entry-header -->
					
						<div class="entry-content">
							<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyeleven' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					</article><!-- #post-0 -->
				</div>
			<?php endif; ?>
			
					
				<div class="small-4 columns">
					<?php get_sidebar('primary'); ?>
				</div>
			</div>

			<section role="related-news" class="show-for-large-only">
				<div class="row collapse">
					<div class="small-12 column">
						<ul class="large-block-grid-4">
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>

			<section role="newsletter">
				<div class="row collapse">
					<div class="small-10 small-centered medium-12 medium-uncentered column">
						<h3 class="text-center uppercase">inscreva-se em nossa newsletter</h3>
						<p class="text-center">
							Nullam id dolor id nibh ultricies vehicula ut id elit. Curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.
						</p>
						<form action="#">
							<div class="row collapse">
								<div class="small-10 columns">
									<input type="text" placeholder="Receba nossas novidades!">
								</div>
								<div class="small-2 columns">
									<a href="#" class="button secondary large postfix">enviar</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</section>

		<section id="primary">
			<div id="content" role="main">

			

			</div><!-- #content -->
		</section><!-- #primary -->
<?php get_footer(); ?>