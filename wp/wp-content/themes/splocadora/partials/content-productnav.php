<?php
	$t         = 0;
	$parts     = 4;
	$terms     = get_terms('product-category');
	$terms_col = array_fill(0, $parts - 1, array());
	$max       = ceil(count($terms) / $parts);

	foreach($terms as $term) {
		count($terms_col[$t]) >= $max and $t ++;
		$terms_col[$t][] = $term;
	}
	// http://stackoverflow.com/questions/15579702/split-an-array-into-n-arrays-php
?>
			<div id="megaMenuProdutos" role="megaMenu">
				<div class="row collapse">
					<div class="medium-12 column">
						<h3 class="icon-tractor">Alugar máquinas e equipamentos para seu dia-a-dia</h3>
					</div>
				</div>
				<div class="row">
					<?php if ( !empty($terms_col) ) : foreach ( $terms_col as &$col ) :?>
						<div class="medium-3 columns">
							<?php foreach ($col as $cat): ?>
								<?php $args = array(
										'post_type'      => 'produtos',
										'post_status'    => 'publish',
										'order'          => 'DESC',
										'orderby'        => 'date',
										'posts_per_page' => 3,
										'tax_query'      => array(
											array(
												'taxonomy' => 'product-category',
												'field'    => 'slug',
												'terms'    => $cat->slug,
											)
										)
									); $query = new WP_Query( $args ); $cat_prods = $query->posts;
								?>
								<h5 class="uppercase"><?= $cat->name ?></h5>
								<ul>
									<?php foreach ($cat_prods as $prod): ?>
										<li><a href="<?= get_permalink($prod->ID); ?>"><?= $prod->post_title ?></a></li>
									<?php endforeach ?>
								</ul>
							<?php endforeach ?>
						</div>
					<?php endforeach; ?>
					<?php else: ?>
						<h5>Sorry, nothing to see here! :(</h5>
					<?php endif; ?>
				</div>
			</div>