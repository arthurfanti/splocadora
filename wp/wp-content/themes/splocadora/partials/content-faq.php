			<section role="faq" <?php echo (!is_front_page()) ? 'class="show-for-small-only"' : 'class="is_home"' ?> >
				<div class="row">
					<div class="small-10 small-centered columns show-for-small-only">
						<fieldset>
							<legend>dúvidas frequentes</legend>
							<dl>
								<i class="icon-question"></i>
								<dt>Etiam</dt>
								<dd>Aenean lacinia bibendum nulla sed consectetur.</dd>
							</dl>
						</fieldset>
					</div>
					<div class="small-10 small-centered medium-3 medium-uncentered columns">
						<div data-interchange="[<?= bloginfo('template_directory'); ?>/images/guia_construcao_small.jpg, (small)], [<?= bloginfo('template_directory'); ?>/images/guia_construcao.jpg, (medium)]">
							<h5 class="uppercase text-center">Guia Completo da Construção Civil</h5>
							<a href="#" class="button expand tiny uppercase">veja mais</a>
						</div>
					</div>
					<div class="small-10 small-centered medium-3 medium-uncentered columns">
						<div data-interchange="[<?= bloginfo('template_directory'); ?>/images/na_obra_small.jpg, (small)], [<?= bloginfo('template_directory'); ?>/images/na_obra.jpg, (medium)]">
							<h5 class="uppercase text-center">Na Obra...</h5>
							<a href="#" class="button expand tiny uppercase">veja mais</a>
						</div>
					</div>
					<div class="small-6 columns show-for-medium-up">
						<fieldset>
							<legend>dúvidas frequentes</legend>
							<dl>
								<i class="icon-doc"></i>
								<dt>Como funciona locação de máquinas?</dt>
								<dd>Saiba todos os detalhes para uma locação bem sucedida</dd>
								<i class="icon-wrench"></i>
								<dt>Qual a multa por atraso na devolução da máquina?</dt>
								<dd>A Focco Locadora é parceira de seus clientes e sempre trabalha com o bom senso.</dd>
								<i class="icon-lock"></i>
								<dt>A máquina apresentou um problema, como procedo?</dt>
								<dd>Entre em contato urgentemente com nosso departamento técnico e traremos a solução o mais rápido possível.</dd>
								<i class="icon-cone"></i>
								<dt>Vocês fazem manutenção de máquinas?</dt>
								<dd>Sim, porém somente dos nossos próprios produtos.</dd>
							</dl>
						</fieldset>
					</div>
				</div>
			</section>