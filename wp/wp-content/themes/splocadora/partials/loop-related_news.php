			<section role="related-news" class="show-for-large-only">
				<div class="row collapse">
					<div class="small-12 column">
						<ul class="large-block-grid-4">
							<?php
								$related_news = new WP_Query(array('post_type' => 'noticias', 'posts_per_page' => 4, 'order' => 'DESC', 'orderby' => 'rand'));
								if ($related_news->have_posts()) : while ($related_news->have_posts()) : $related_news->the_post();
							?>
							<li>
								<?php if (has_post_thumbnail()) : the_post_thumbnail('news-middle'); else: ?>
									<img src="//placehold.it/330x268/F0F0F0&text=Imagem Indisponível" alt="<?php the_title(); ?>">
								<?php endif ?>
								<p><?php the_title(); ?></p>
								<a href="<?= get_post_permalink(); ?>" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
							<?php else : ?>
							<h2 class="n_encontrado">Não encontrado!</h2>
							<p class="n_encontrado">Desculpe, mas não encontramos o que você procura.</p>
							<br/>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</section>