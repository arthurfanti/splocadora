<?php
	$fields     = get_fields();
	$bannerList = array();
	
	foreach ($fields as $field) {
		array_push($bannerList, $field);
	}

	$bannerList = array_chunk($bannerList, 3);
 ?>
 <?php foreach ($bannerList as $banner): ?>
 	<?php if ( !wp_is_mobile() ): ?>
 		<li style="background-image: url( <?= $banner[0] ?> )">
			<div class="orbit-caption text-right clearfix">
				<?= $banner[2] ?>
				<a href="#" class="button expand round right uppercase show-for-medium-up">mais detalhes</a>
				<a href="#" class="button expand round right tiny uppercase show-for-small-only">mais detalhes</a>
			</div>
 		</li>
 	<?php else: ?>
 		<li style="background-image: url( <?= $banner[1] ?> )">
			<div class="orbit-caption text-right clearfix">
				<?= $banner[2] ?>
				<a href="#" class="button expand round right uppercase show-for-medium-up">mais detalhes</a>
				<a href="#" class="button expand round right tiny uppercase show-for-small-only">mais detalhes</a>
			</div>
 		</li>
 	<?php endif ?>
 <?php endforeach ?>