			<section role="historia">
				<div class="row">
					<div class="small-12 columns">
						<h3 class="text-center">
							<span class="primary">FOCCO LOCADORA -</span>
							EXCELÊNCIA NO MERCADO DE LOCADORES DE MÁQUINAS E EQUIPAMENTOS
						</h3>
					</div>
					<div class="small-12 medium-3 columns">
						<!--<img data-interchange="[//placehold.it/350x100/212121/F47C20.png&text=empresa.png, (default)], [//placehold.it/275x202/333/F47C20.png&text=empresa.png, (medium)]" alt="">
						<noscript><img src="//placehold.it/275x202.png" alt=""></noscript>-->
						<img src="<?= bloginfo('template_directory'); ?>/images/focco-locadora-de-maquinas-e-equipamentos.jpg" alt="Focco Locadora de Máquinas e Equipamentos" />
					</div>
					<div class="small-12 small-only-text-justify medium-9 columns">
						<p>
							A FOCCO LOCADORA DE MÁQUINAS atua no ramo de aluguel de máquinas e equipamentos para a construção civil com excelência máxima na prestação de serviços. Durante esses anos, a empresa aperfeiçoou toda a sua estrutura operacional, garantindo uma entrega com agilidade e segurança, além de produtos de qualidade. <br />
							A lista de produtos da Focco Locadora inclui diversos tipos de <strong>locação de máquinas e equipamentos</strong> para obras. Desde o aluguel de máquinas de concretagem, como betoneiras, até ferramentas elétricas e pneumáticas. Tudo com a melhor qualidade do mercado e sempre produtos com as mais recentes tecnologias.

						</p>
						<div class="text-center">
							<a href="<?= get_permalink( get_page_by_path('focco-locadora-de-maquinas-e-equipamentos') ); ?>" class="button radius uppercase">conheça nossa história</a>
						</div>
					</div>
				</div>
			</section>