			<section role="newsletter">
				<div class="row collapse">
					<div class="small-10 small-centered medium-12 medium-uncentered column">
						<h3 class="text-center uppercase">inscreva-se em nossa newsletter</h3>
						<p class="text-center">
							Ao inserir seu email você receberá automaticamente 10% de desconto na locação de uma de nossas máquinas. Além disso periodicamente receberá novidades em nosso portfólio de produtos.
						</p>
						
						<form method="post" action="#" id="subscribe">
							<input type="hidden" name="sp_list" value="170"/>
							<input type="hidden" name="sendpress" value="post" />
							<div class="row collapse">
								<div class="small-10 columns">
									<input type="text" value="" placeholder="Receba nossas novidades!" name="sp_email"/>
								</div>
								<div class="small-2 columns">
									<a href="/submit" class="button secondary large postfix" id="submit" name="submit">Enviar</a>
								</div>
							</div>
						</form>
								
					</div>
				</div>
			</section>
			<script>
				jQuery(document).ready(function($) {
					$('#subscribe').submit(function(event) {
						event.preventDefault();
						var data  = $('#subscribe').serializeArray();
						var jqxhr = jQuery.post('http://' + '<?= $_SERVER["SERVER_NAME"] ?>', data, function(data, textStatus, xhr) {
							$('div[data-alert].success').fadeIn('fast').append('&nbsp;Em breve você receberá nossas novidades em&nbsp;' + data.email);
						}).done(function(){
							// console.log("something fine");
							$('#subscribe').find('a').text('Enviar');
						}).fail(function(){
							// console.log("something wrong");
							$('div[data-alert].warn').fadeIn('fast');
						});
						
					});

					$('#subscribe').find('a').bind('click', function(event) {
						event.preventDefault();
						$(this).text('aguarde...');
						$('#subscribe').submit();
					});
				});
			</script>