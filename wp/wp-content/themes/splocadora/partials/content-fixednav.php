<?php
	$terms = get_terms("product-category");

	$args_news = array('post_type' => 'noticias', 'post_status' => 'published', 'order' => 'DESC', 'orderby' => 'date', 'posts_per_page' => 5, 'nopaging' => false, 'perm' => 'readable', 'no_found_rows' => false, 'cache_results' => true, 'update_post_term_cache' => true, 'update_post_meta_cache' => true, );
	$query_news = new WP_Query( $args_news );

	// $args_filiais = array('post_type' => 'filial', 'post_status' => 'published', 'order' => 'DESC', 'orderby' => 'date', 'posts_per_page' => 3, 'nopaging' => false, 'perm' => 'readable', 'no_found_rows' => false, 'cache_results' => true, 'update_post_term_cache' => true, 'update_post_meta_cache' => true, );
	// $query_filiais = new WP_Query( $args_filiais );
	// wp_reset_query();
?>
		<section role="fixednav">
			<div data-magellan-expedition="fixed">
				<div class="row collapse">
					<div class="small-2 small-push-1 medium-3 medium-push-0 columns">
						<ul data-magellan-destination="menu" class="sub-nav">
							<li data-magellan-arrival="menu">
								<a href="#" class="icon-hamburger"></a>
							</li>
							<li class="show-for-medium-up">
								<a data-orbit-slide="prev" href="#" class="icon-prev"></a>
							</li>
							<li class="show-for-medium-up">
								<a data-orbit-slide="next" href="#" class="icon-next"></a>
							</li>
						</ul>
					</div>

					<div class="small-7 small-pull-1 medium-4 medium-pull-0 columns">
						<ul class="sub-nav right">
							<!-- <li><a href="#" class="icon-twitter"></a></li> -->
							<li><a href="#" class="icon-facebook"></a></li>
							<li class="show-for-medium-up"><a href="mailto:locacao@foccolocadora.com.br" class="icon-mail"></a></li>
							<li class="<?= (is_singular('produtos') || is_page('orcamento')) ? 'disabled' : 'enabled' ; ?>" id="quickBudget"><a href="#" class="icon-cart"></a></li>
							<li><a href="#" class="icon-search" id="trigger-overlay"></a></li>
							<script>jQuery(document).ready(function($) {
								$('#trigger-overlay').click(function(event) {
									event.preventDefault();
									$(' .overlay form[role=search] input[type=text] ').focus();
								});
							});</script>
						</ul>
					</div>
				</div>

				<div id="orcamento-rapido" class="budget <?= (is_singular('produtos') || is_page('orcamento')) ? 'disabled' : 'enabled' ; ?>">
					<div id="titulo">
						<p>Orçamento Rápido</p>
						<span class="total-amount right"></span>
					</div>
					<div id="produtos"></div>
					<div id="bts-conclusao">
						<a class="button tiny secondary uppercase solicitar" href="<?php echo esc_url(get_permalink( get_page_by_title('orcamento') )) ?>">Solicitar Orçamento</a>
					</div>
				</div>

				<div id="megaMenuMainNav" role="megaMenu">
					<div class="row" role="siteMap">
						<div class="small-6 small-centered medium-2 medium-uncentered columns">
							<nav>
								<ul>
									<li>
										<h5 class="uppercase"><a href="<?= get_permalink( get_page_by_path('home') ); ?>">home</a></h5>
									</li>
									<li>
										<h5 class="uppercase"><a href="<?= get_permalink( get_page_by_path('focco-locadora-de-maquinas-e-equipamentos') ); ?>">focco locadora</a></h5>
									</li>
									<li>
										<h5 class="uppercase"><a href="<?= get_permalink( get_page_by_path('orcamento') ); ?>">orçamento</a></h5>
									</li>
									<li>
										<h5 class="uppercase"><a href="<?= get_permalink( get_page_by_path('contato') ); ?>">contato</a></h5>
									</li>
								</ul>
							</nav>
						</div>
						<div class="medium-4 columns show-for-medium-up">
							<nav>
								<ul>
									<li>
										<h5 class="uppercase"><a href="<?= get_site_url() . '/produtos' ?>">Produtos</a></h5>
									</li>
									<?php if ( !empty($terms) && !is_wp_error($terms) ): foreach ($terms as $term): ?>
										<li><a href="<?= get_term_link($term) ?>"><?= $term->name ?></a></li>
									<?php endforeach; endif ?>
								</ul>
							</nav>
						</div>
						<div class="medium-4 columns show-for-medium-up">
							<nav>
								<ul>
									<li>
										<h5 class="uppercase"><a href="<?= get_site_url() . '/noticias' ?>">Notícias</a></h5>
									</li>
									<?php if ( $query_news->have_posts() ) : while ( $query_news->have_posts() ) : $query_news->the_post(); ?>
									<li><a href="<?= the_permalink() ?>"><?= the_title() ?></a></li>
									<?php endwhile; ?>
									<!-- post navigation -->
									<?php wp_reset_postdata(); ?>
									<?php else: ?>
									<li>Sorry, nothing to see here! :(</li>
									<?php endif; ?>
									<?php wp_reset_query(); ?>
									<?php wp_reset_postdata(); ?>
								</ul>
							</nav>
						</div>
						<div class="medium-2 columns show-for-medium-up">
							
						</div>
					</div>
				</div>
			</div>
		</section>