<?php
	$args        = array( 'post_type' => 'filial' );
	$posts_array = get_posts( $args );
	// echo "<pre>";
	// foreach ($posts_array as $filial) {
	// 	print_r($filial);
	// 	print_r( get_post_meta($filial->ID) );
	// }
	// echo "</pre>";
	
 ?>

 <?php foreach ($posts_array as $filial): ?>
 <?php
 	$adress_pieces  = get_post_meta( $filial->ID, 'adress_final', true );
 	$adress_pieces  = explode("|", $adress_pieces);
 ?>
<li>
	<?php if ('' != get_the_post_thumbnail($filial->ID)): ?>
		<?= get_the_post_thumbnail($filial->ID) ?>
	<?php else: ?>
		<img src="//placehold.it/323x121/F0F0F0/F47C20.png&text=<?= $filial->post_title ?>" alt="">
	<?php endif ?>
	<div class="vcard">
	<h5 class="fn uppercase"><?= $filial->post_title; ?></h5>
		<p class="adr">
			<span class="street-address"><?= "Endereço: " . $adress_pieces[0] ?></span><br>
			<span class="region"><?= $adress_pieces[1] . " - " . $adress_pieces[2] . " - " . $adress_pieces[3] ?></span><br>
			<span class="postal-code"><?= "CEP: " . $adress_pieces[4] ?></span><br>
			<span class="tel"><?= "Telefone: " . $adress_pieces[5] ?></span><br>
			<span class="fax"><?= "Fax: " . $adress_pieces[6] ?></span><br>
			<span class="email"><?= "Email: " . $adress_pieces[7] ?></span>
		</p>
		<a href="#" class="mapLink" data-lat="<?= $adress_pieces[8] ?>" data-lon="<?= $adress_pieces[9] ?>">ver no mapa</a>
	</div>
</li>
 <?php endforeach ?>