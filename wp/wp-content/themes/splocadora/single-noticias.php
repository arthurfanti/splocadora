<?php get_header('internas'); ?>
			<article role="newsContent">
				<div class="row">
					<div class="small-8 small-centered medium-incentered medium-12 columns">
						<?php while ( have_posts() ) : the_post(); ?>
						<h1>
							<em class="show-for-small-only" id="authorInfo"><?= get_the_date(); ?> // publicado por <?php the_author(); ?></em>
							<?php the_title(); ?>
						</h1>
					</div>
					<div class="small-8 small-centered medium-uncentered columns">
							<em class="show-for-medium-up" id="authorInfo"><?php the_date(); ?> // publicado por <?php the_author(); ?></em>
							<?php the_post_thumbnail(); ?>
							<?php the_content(); ?>
						<?php endwhile; // end of the loop. ?>
					</div>
					<div class="medium-4 show-for-medium-up columns">
						<?php get_sidebar('primary'); ?>
					</div>
				</div>
			</article>
			

			<?php get_template_part('partials/loop', 'related_news'); ?>

			<?php get_template_part('partials/content', 'faq'); ?>

			<?php get_template_part('partials/content', 'about'); ?>

			<?php get_template_part('partials/content', 'subscribe'); ?>
<?php get_footer(); ?>