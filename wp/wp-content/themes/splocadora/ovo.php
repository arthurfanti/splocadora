<?php
	error_reporting(E_ALL);
	if (isset($_GET['action'])) {
		$formData = json_decode(file_get_contents('php://input'), true);
		switch ($_GET['action']) {
			case 'budgetForm':
				header("HTTP/1.0 200 OK");

				try {
					$name  = $formData["name"];
					$email = $formData["email"];

					define('CONTACT_EMAIL', 'arthur@dominidesign.com.br');

					$message  = "<table cellpadding='16' border='0'>";
					$message .= "<tr><td><b>Empresa:</b></td><td>" . $formData["corp"] . "</td></tr>";
					$message .= "<tr><td><b>Nome de Contato:</b></td><td>" 	. $formData["name"] . "</td></tr>";
					$message .= "<tr><td><b>CNPJ</b></td><td>" . $formData["cnpj"] . "</td></tr>";
					$message .= "<tr><td><b>Endereço</b></td><td>" . $formData["address"] . "</td></tr>";
					$message .= "<tr><td><b>Número</b></td><td>" . $formData["address_number"] . "</td></tr>";
					$message .= "<tr><td>";
					$message .= "<tr><td><b>Email</b></td><td>" . $formData["email"] . "</td></tr>";
					$message .= "<tr><td><b>Telefone Fixo</b></td><td>(" . $formData["ddd"] . ") " . $formData["phone"] . "</td></tr>";
					$message .= "<tr><td><b>Observações</b></td><td><br>" . $formData["obs"] . "<br></td></tr>";
					$message .= "</table>";

					if (isset($formData["prodData"])) {
						$message .= '<br><br><br>';
						$message .= '<h2>Produtos:</h2>';
						$message .= '<table cellpadding="16" border="0"><tr><td colspan="2">';
						foreach ($formData["prodData"] as $id => $prodData) {
								$message .= '<div style="width:100px;display:inline-block;margin:12px;">';
								$message .= '	<h3 style="margin-bottom: 4px;">' . $prodData["name"] . '</h3>';
								$message .= '	<span><b>Quantidade: </b>' . $prodData["qt"] . '</span><br><br>';
								$message .= '	<center><img src="' . $prodData["thumbUrl"] . '" alt="' . $prodData["name"] . '" width="36" height="36"></center>';
								$message .= '	<center><a href="' . $prodData["link"] . '">Ver Produto</a></center>';
								$message .= '</div>';
						}
						$message .= "</td></tr></table>";
					}

					$message .= '<h2>Periodo de Locação:</h2>';
					$message .= '<span>De ' . $formData["from_date"] . ' até ' . $formData["to_date"];

				} catch (Exception $e) {
					echo "error";
				}

				if ( @mail('arthur@dominidesign.com.br', 'form from hell', $message) ) { print_r($message); }
				break;
			
			default:
				echo "invalid action";
				break;
		}
	}
?>