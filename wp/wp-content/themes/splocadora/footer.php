		</div>
		<footer>
			<div class="row show-for-medium-up">
				<div class="small-12 column">
					<p class="lead">
						A lista de produtos da Focco Locadora inclui diversos tipos de locação de máquinas e equipamentos para obras. Desde o aluguel de máquinas de concretagem, como betoneiras, até ferramentas elétricas e pneumáticas. Tudo com a melhor qualidade do mercado e sempre produtos com as mais recentes tecnologias.
					</p>
				</div>
				<ul data-equalizer role="siteMap" class="medium-block-grid-4">
					<li data-equalizer-watch>
						<nav>
							<ul>
								<li><h5 class="uppercase"><a href="http://www.foccolocadora.com.br/">home</a></h5></li>
								<li><h5 class="uppercase"><a href="http://www.foccolocadora.com.br/focco-locadora-de-maquinas-e-equipamentos/">focco locadora</a></h5></li>
								<li><h5 class="uppercase"><a href="http://www.foccolocadora.com.br/orcamento/">orçamento</a></h5></li>
								<li><h5 class="uppercase"><a href="http://www.foccolocadora.com.br/contato/">contato</a></h5></li>
							</ul>
						</nav>
					</li>
					<li data-equalizer-watch>
						<nav>
							<ul>
								<li>
									<h5 class="uppercase">
										<a href="<?= get_post_type_archive_link('produtos'); ?>">Produtos</a>
									</h5>
								</li>
							<?php
								$terms = get_terms('product-category');
								if (!empty($terms)) {
									foreach ($terms as $term) {
										$term_link = get_term_link($term);
										if ( is_wp_error($term_link) ) {
											continue;
										}
										echo '<li><a href="' . esc_url($term_link) . '">' . $term->name . '</a></li>';
									}
								}
							?>
							</ul>
						</nav>
					</li>
					<li data-equalizer-watch>
						<nav>
							<ul>
								<li>
									<h5 class="uppercase">
										<a href="<?= get_post_type_archive_link('noticias'); ?>">Notícias</a>
									</h5>
								</li>
								<?php query_posts("post_type=noticias&showposts=3");
								if (have_posts()) : while (have_posts()) : the_post();  ?>
								<li>
								<a href="<?= get_post_permalink(); ?>" title="<?= the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; ?>
								<?php else : ?>
								<h2 class="n_encontrado">Não encontrado!</h2>
								<p class="n_encontrado">Desculpe, mas não encontramos o que você procura.</p>
								<br/>
								<?php endif; ?>
							</ul>
						</nav>
					</li>
					<li data-equalizer-watch>
						<nav>
							<ul>
								<li>
									<h5 class="uppercase"><a href="#">Mais alugados</a></h5>
								</li>
								<li><a href="#">Locação de mini gruas</a></li>
								<li><a href="#">Locação de andaimes</a></li>
								<li><a href="#">Locação de empilhadeiras</a></li>
							</ul>
						</nav>
					</li>
				</ul>
			</div>
			<section role="footerInfo" class="row">
				<div class="small-8 small-centered columns show-for-small-only">
					<nav>
						<ul>
							<li><h5 class="uppercase"><a href="http://www.foccolocadora.com.br/">home</a></h5></li>
							<li><h5 class="uppercase"><a href="http://www.foccolocadora.com.br/focco-locadora-de-maquinas-e-equipamentos/">focco locadora</a></h5></li>
							<li><h5 class="uppercase"><a href="http://www.foccolocadora.com.br/orcamento/">orçamento</a></h5></li>
							<li><h5 class="uppercase"><a href="http://www.foccolocadora.com.br/contato/">contato</a></h5></li>
						</ul>
					</nav>
				</div>

				<div class="small-8 small-centered medium-4 medium-push-4 medium-uncentered columns medium-text-center">
					<i class="icon-info show-for-medium-up"></i>
					<small>
						LOCAÇÕES <em class="primary">0800 770-8300</em><br>
						Tel: +55 (11) 3502 - 7230<br>
						<a href="mailto:locacao@foccolocadora.com.br" style="color:#F47C20; font-size:13px;">locacao@foccolocadora.com.br</a>
					</small>
				</div>

				<div class="small-8 small-centered medium-3 medium-push-3 medium-uncentered columns medium-text-center">
					<h6 class="uppercase primary">siga-nos</h6>
					<div class="row social-icons">
						<div class="small-2 medium-offset-2 columns"><a href="#" class="icon-instagram"></a></div>
						<div class="small-2 columns"><a href="#" class="icon-facebook"></a></div>
						<!-- <div class="small-2 columns"><a href="#" class="icon-twitter"></a></div> -->
						<div class="small-2 end columns"><a href="#" class="icon-feed"></a></div>
					</div>
				</div>

				<div class="small-8 small-centered medium-3 medium-pull-6 medium-uncentered columns medium-text-center">
					<i class="icon-pin show-for-medium-up"></i>
					<small>Rua Carlos Pavan, 172 / 05537-090 / Jardim Peri Peri / São Paulo / Brasil</small>
				</div>
			</section>

			<section role="copyright" class="row collapse">
				<div class="small-12 column text-center">
					<small>Focco Locadora @ 2014 Todos os direitos reservados</small>
				</div>
			</section>

			<section role="footerNews" class="row collapse show-for-medium-up">
				<div class="small-5 columns text-center">
					<span class="primary uppercase">acontece na focco locadora</span>
				</div>
				<div class="small-7 columns">
					<em class="uppercase">
						Locação de andaimes: mais segurança e economia
						<small>
							<a href="http://www.foccolocadora.com.br/noticias/locacao-de-andaimes-mais-seguranca-e-economia/">saiba mais</a>
						</small>
					</em>
				</div>
			</section>
		</footer>
		<div class="overlay overlay-hugeinc">
			<button type="button" class="overlay-close">Close</button>
			<?php get_search_form(); ?>
		</div>
		<?php wp_footer(); ?>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-57104667-1', 'auto');
			ga('send', 'pageview');
		</script>
	</body>
</html>