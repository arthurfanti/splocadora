<?php
	$photos    = rwmb_meta('spl_plupload', 'type=plupload_image&size=large');
	$thumbs    = rwmb_meta('spl_plupload', 'type=plupload_image&size=thumbnail');
	$cat_array = get_the_terms($post->ID, 'product-category');
	$cat_array = array_shift($cat_array);
	$category  = $cat_array->name;
	$d_tecs    = rwmb_meta('spl_dados_tecnicos');

	if (has_post_thumbnail()) {
		$_prod_thumbs = wp_get_attachment_image_src(get_post_thumbnail_id(), array(36,36));
		$_prod_thumb  = $_prod_thumbs[0];
	} else {
		$_prod_thumb = "";
	}
?>
<?php get_header('internas'); ?>
			<article role="prodContent">
				<div class="row">
					<div class="small-12 columns">
						<!-- <h4><em>compartilhe</em></h4> -->
						<br>
					</div>
					<div class="small-8 small-centered medium-uncentered columns">
						<div class="row collapse">
							<div class="small-12 columns">
								<h1><?php if ( have_posts() ) : while ( have_posts() ) : the_post(); the_title(); endwhile; endif; ?></h1>
								<p class="lead"><?= $category; ?></p>
							</div>
							<hr>
							<div class="small-12 columns">
								<a href="#" class="button button-icon tiny secondary radius uppercase add-to-cart-button" style="margin-top:2rem; font-size:0.75rem; padding:0.25rem 1.25rem;" data-prod-info='{"id":"<?php the_ID(); ?>","name":"<?php the_title(); ?>","link":"<?php the_permalink() ?>","thumbUrl":"<?php echo $_prod_thumb; ?>"}'>
									adicionar ao orçamento
									<i class="icon-cart"></i>
								</a>
							</div>
							<!-- <div class="small-2 medium-1 columns">
								<input class="radius" type="number" value="1">
							</div> -->
							<div class="small-12 column">
								<?php if (!wp_is_mobile()): ?>
									<ul class="spl_plupload" data-orbit data-options="animation:fade; slide_number:false; navigation_arrows:false; next_on_click:false; bullets:false; timer:false">
									<?php if (!empty($photos)) : foreach ($photos as $photo): ?>
										<li data-orbit-slide="<?= $photo['title'] ?>">
											<img src="<?= $photo['url'] ?>" alt="<?= $photo['alt'] ?>" />
										</li>
									<?php endforeach ?>
									</ul>

									<?php foreach ($thumbs as $thumb): ?>
									<a data-orbit-link="<?= $thumb['title'] ?>">
										<img src="<?= $thumb['url'] ?>">
									</a>
									<?php endforeach; else : ?>
									<img src="http://placehold.it/1024x678/F9F9F9&text=Imagem Indisponível" alt="<?php the_title(); ?>">
								<?php endif; else: ?>
									<ul class="spl_plupload" data-orbit data-options="animation:fade; slide_number:false; navigation_arrows:false; next_on_click:true; bullets:true; timer:false; variable_height: false">
									<?php foreach ($photos as $photo): ?>
										<li data-orbit-slide="<?= $photo['title'] ?>">
											<img src="<?= $photo['url'] ?>" alt="<?= $photo['alt'] ?>" />
										</li>
									<?php endforeach ?>
									</ul>
								<?php endif ?>

								<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
									<?php the_content(); ?>
								<?php endwhile; endif; ?>

								<div class="panel panel-data radius">
								  <h5>Dados Técnicos</h5>
								  <div><?= $d_tecs; ?></div>
								</div>
							</div>
							<hr>
							<div class="small-10 medium-4 end columns">
								<a href="#" class="button button-icon tiny secondary radius uppercase add-to-cart-button" style="margin-top:2rem; font-size:0.75rem; padding:0.25rem 1.25rem;" data-prod-info='{"id":"<?php the_ID(); ?>","name":"<?php the_title(); ?>","link":"<?php the_permalink() ?>","thumbUrl":"<?php echo $_prod_thumb; ?>"}'>
									adicionar ao orçamento
									<i class="icon-cart"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="medium-4 show-for-medium-up columns">
						<div id="orcamento-rapido">
							<div id="titulo">
								<p>Orçamento Rápido</p>
								<span class="total-amount right"></span>
							</div>
							<div id="produtos"></div>
							<div id="bts-conclusao">
								<a class="button tiny secondary uppercase solicitar" href="<?php echo esc_url(get_permalink( get_page_by_title('orcamento') )) ?>">Solicitar Orçamento</a>
							</div>
						</div>
						<?php get_sidebar('primary'); ?>
					</div>
				</div>
			</article>

			<?php get_template_part('partials/content', 'faq'); ?>

			<?php get_template_part('partials/content', 'subscribe'); ?>
<?php get_footer(); ?>
