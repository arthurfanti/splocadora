<?php

function theme_scripts() {
		$hash = hash('adler32', uniqid(rand(), true));
		$myvars = array( 
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		);

	wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/vendor/modernizr.min.js', array() );
	wp_enqueue_script('libraries', get_template_directory_uri() . '/js/vendor/libraries.min.js', array('jquery'), null, true );
	wp_enqueue_script('foundation', get_template_directory_uri() . '/js/vendor/foundation.min.js', array('jquery'), null, true );
	wp_enqueue_script('app', get_template_directory_uri() . '/js/app.min.js', null, $hash, true );
	wp_localize_script( 'app', 'MyAjax', $myvars );

	if ( preg_match('/filiais/', $_SERVER['REQUEST_URI']) ) {
		wp_enqueue_script('maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyAhalXSnKDX5-ZYXlAB9D0sdOzQgRayMNw&sensor=false', array() );
	}
}

function theme_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}

add_action( 'init', 'wptp_register_taxonomies', 0 );
function wptp_register_taxonomies() {
	 
	// register_taxonomy( 'categoria', 'produtos',
	// 	array(
	// 		'labels' => array(
	// 			'name'          => 'Categorias',
	// 			'singular_name' => 'Categoria',
	// 			'search_items'  => 'Buscar Categorias',
	// 			'all_items'     => 'Todos Categorias',
	// 			'edit_item'     => 'Editar Categoria',
	// 			'update_item'   => 'Atualizar Categoria',
	// 			'add_new_item'  => 'Incluir Categoria',
	// 			'new_item_name' => 'Novo nome de Categoria',
	// 			'menu_name'     => 'Categoria',
	// 		),
	// 		'rewrite'      => array('slug' => 'produtos/categoria', 'with_front' => true ),
	// 		'hierarchical' => true,
	// 		'has_archive'  => true,
	// 		'sort' => true,
	// 		'args' => array( 'orderby' => 'term_order' ),
	// 		'show_admin_column' => true
	// 	)
	// );
	$labels = array(
		'name'              => _x( 'Categorias de Produto', 'taxonomy general name' ),
		'singular_name'     => _x( 'Categoria de Produto', 'taxonomy singular name' ),
		'search_items'      => __( 'Buscar Categorias' ),
		'all_items'         => __( 'Todas Categorias' ),
		'parent_item'       => __( 'Categoria Pai' ),
		'parent_item_colon' => __( 'Categoria Pai:' ),
		'edit_item'         => __( 'Editar Categoria' ), 
		'update_item'       => __( 'Atualizar Categoria' ),
		'add_new_item'      => __( 'Incluir Categoria' ),
		'new_item_name'     => __( 'Nova da nova categoria' ),
		'menu_name'         => __( 'Categorias' )
	);  
	
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'produtos/categoria' )
	);
	
	register_taxonomy( 'product-category', array( 'produtos' ), $args );
}

function wptp_create_post_type() {
	// $labels_filiais = array( 
	// 	'name'               => __( 'Filiais' ),
	// 	'singular_name'      => __( 'filial' ),
	// 	'add_new'            => __( 'Criar filial' ),
	// 	'add_new_item'       => __( 'Add New filial' ),
	// 	'edit_item'          => __( 'Edit filial' ),
	// 	'new_item'           => __( 'New filial' ),
	// 	'view_item'          => __( 'View filial' ),
	// 	'search_items'       => __( 'Search Filiais' ),
	// 	'not_found'          =>  __( 'No Filiais Found' ),
	// 	'not_found_in_trash' => __( 'No filiais found in Trash' ),
	// );
	// $args_filiais = array(
	// 	'labels'       => $labels_filiais,
	// 	'has_archive'  => false,
	// 	'public'       => true,
	// 	'hierarchical' => false,
	// 	'menu_icon'    => 'dashicons-networking',
	// 	'supports'     => array(
	// 		'title',
	// 		'thumbnail',
	// 		// 'editor',
	// 		// 'excerpt',
	// 		// 'custom-fields',
	// 		// 'page-attributes'
	// 	),
	// 	// 'taxonomies' => array( 'post_tag', 'category'), 
	// );

	$labels_noticias = array( 
		'name'               => __( 'Noticias' ),
		'singular_name'      => __( 'noticia' ),
		'add_new'            => __( 'Criar noticia' ),
		'add_new_item'       => __( 'Add New noticia' ),
		'edit_item'          => __( 'Edit noticia' ),
		'new_item'           => __( 'New noticia' ),
		'view_item'          => __( 'View noticia' ),
		'search_items'       => __( 'Search Noticias' ),
		'not_found'          =>  __( 'No Noticias Found' ),
		'not_found_in_trash' => __( 'No noticias found in Trash' ),
	);
	$args_noticias = array(
		'labels'       => $labels_noticias,
		'has_archive'  => true,
		'public'       => true,
		'hierarchical' => true,
		'menu_icon'    => 'dashicons-format-quote',
		'taxonomies'   => array( 'post_tag', 'category'), 
		'supports'     => array(
			'title',
			'thumbnail',
			'editor',
			'excerpt',
			// 'custom-fields',
			// 'page-attributes'
		),
	);

	$labels_produtos = array( 
		'name'               => __( 'Produtos' ),
		'singular_name'      => __( 'produto' ),
		'add_new'            => __( 'Criar produto' ),
		'add_new_item'       => __( 'Add New produto' ),
		'edit_item'          => __( 'Edit produto' ),
		'new_item'           => __( 'New produto' ),
		'view_item'          => __( 'View produto' ),
		'search_items'       => __( 'Search Produtos' ),
		'not_found'          =>  __( 'No Produtos Found' ),
		'not_found_in_trash' => __( 'No produtos found in Trash' ),
	);
	$args_produtos = array(
		'labels'       => $labels_produtos,
		'has_archive'  => 'produtos',
		'public'       => true,
		'hierarchical' => true,
		'menu_icon'    => 'dashicons-cart',
		// 'taxonomies'   => array( 'post_tag', 'category'),
		'rewrite'      => array('slug' => 'produto', 'with_front' => true ),
		'supports'     => array(
			'title',
			'thumbnail',
			'editor',
			'excerpt',
			// 'custom-fields',
			// 'page-attributes'
		),
	);

	// register_post_type( 'filial', $args_filiais );
	register_post_type( 'noticias', $args_noticias );
	register_post_type( 'produtos', $args_produtos );
	flush_rewrite_rules(false);
}

function add_custom_meta_boxes() {
	add_meta_box(
		'adress_box',
		'Endereço da Filial',
		'display_adress_box',
		'filial',
		'advanced',
		'high'
	);
}

function display_adress_box( $adress_final, $adress_final_format ) {
	global $post;
	$post_id = $post->ID;

	$adress_final        = get_post_meta( $post_id, 'adress_final', true  );
	$adress_final_format = get_post_meta( $post_id, 'adress_final_format', true  );
	$adress_pieces       = explode("|", $adress_final);

	wp_nonce_field(plugin_basename(__FILE__), 'adress_final_nonce');

	$html = '<div id="adress">';
	$html .= '<p style="display:inline-block; width:50%;">Endereço:&nbsp;';
	$html .= '<input value="' . $adress_pieces[0] . '" style="display:block; width:99%;" type="text" id="adress_endereco" name="adress_endereco" />';
	$html .= '</p>';

	$html .= '<p style="display:inline-block; width:35%;">Cidade:&nbsp;';
	$html .= '<input value="' . $adress_pieces[1] . '" style="display:block; width:99%;" type="text" id="adress_cidade" name="adress_cidade" />';
	$html .= '</p>';

	$html .= '<p style="display:inline-block; width:14%;">UF:&nbsp;';
	$html .= '<input value="' . $adress_pieces[2] . '" style="display:block; width:100%;" type="text" id="adress_uf" name="adress_uf" maxlength="2" />';
	$html .= '</p>';

	$html .= '<p style="display:inline-block; width:25%;">País:&nbsp;';
	$html .= '<input value="' . $adress_pieces[3] . '" style="display:block; width:99%;" type="text" id="adress_pais" name="adress_pais" />';
	$html .= '</p>';

	$html .= '<p style="display:inline-block; width:25%;">CEP:&nbsp;';
	$html .= '<input value="' . $adress_pieces[4] . '" style="display:block; width:99%;" type="text" id="adress_cep" name="adress_cep" />';
	$html .= '</p>';

	$html .= '<p style="display:inline-block; width:25%;">Telefone:&nbsp;';
	$html .= '<input value="' . $adress_pieces[5] . '" style="display:block; width:99%;" type="text" id="adress_tel" name="adress_tel" placeholder="(99)9999-9999" />';
	$html .= '</p>';

	$html .= '<p style="display:inline-block; width:25%;">FAX:&nbsp;';
	$html .= '<input value="' . $adress_pieces[6] . '" style="display:block; width:99%;" type="text" id="adress_fax" name="adress_fax" placeholder="(99)9999-9999" />';
	$html .= '</p>';

	$html .= '<p style="display:inline-block; width:50%;">Email:&nbsp;';
	$html .= '<input value="' . $adress_pieces[7] . '" style="display:block; width:99%;" type="text" id="adress_email" name="adress_email" />';
	$html .= '</p>';

	$html .= '<p style="display:inline-block; width:25%;">Latitude:&nbsp;';
	$html .= '<input value="' . $adress_pieces[8] . '" style="display:block; width:99%;" type="text" id="adress_lat" name="adress_lat" />';
	$html .= '</p>';

	$html .= '<p style="display:inline-block; width:25%;">Longitude:&nbsp;';
	$html .= '<input value="' . $adress_pieces[9] . '" style="display:block; width:99%;" type="text" id="adress_long" name="adress_long" />';
	$html .= '</p>';
	$html .= '</div>';
	
	$html .= '<input type="button" id="btn_adress" value="salvar endereço" class="button button-large" />';

	$html .= "<input type='hidden' name='adress_final' id='adress_final' value='" . $adress_final . "' />";

	$html .= "<script>\n";
	$html .="jQuery(document).ready(function($) {\n";
	$html .="	$('#btn_adress').click(function(event) {\n";
	$html .="		var value = '';\n";
	$html .="		$('input[id^=\"adress_\"][type=\"text\"]').each(function(index, el) {\n";
	$html .="			value += $(this).val() + '|';\n";
	$html .="		});\n";
	$html .="		$('#adress_final').val(value);\n";
	$html .="		alert(\"Endereço salvo com sucesso!\")";
	$html .="	});\n";
	$html .="});\n";
	$html .="</script>";

	echo $html;
	echo $adress_final_format;

}

function save_custom_meta_data($id) {

	if ( isset($_POST['post_type']) && "filiais" == $_POST['post_type'] ) {
		if(!wp_verify_nonce($_POST['adress_final_nonce'], plugin_basename(__FILE__))) {
		  return $id;
		}
		   
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		  return $id;
		}
		   
		if('page' == $_POST['post_type']) {
		  if(!current_user_can('edit_page', $id)) {
			return $id;
		  }
		} else {
			if(!current_user_can('edit_page', $id)) {
				return $id;
			}
		}

		if (!empty( $_POST['adress_final'] )) {
			$adress_pieces  = explode("|", $_POST['adress_final']);
			$adress_format  = "<p>";
			$adress_format  .= "Endereço: " . $adress_pieces[0] . "<br />";
			$adress_format  .= $adress_pieces[1] . "&nbsp;" . $adress_pieces[2] . "&nbsp;" . $adress_pieces[3] . "<br />";
			$adress_format  .= "CEP: " . $adress_pieces[4] . "<br />";
			$adress_format  .= "Telefone: " . $adress_pieces[5] . "<br />";
			$adress_format  .= "Fax: " . $adress_pieces[6] . "<br />";
			$adress_format  .= "Email: " . $adress_pieces[7] . "<br />";
			$adress_format  .= "</p>";

			update_post_meta($id, 'adress_final', $_POST['adress_final']);
			update_post_meta($id, 'adress_final_format', $adress_format);
		}
	}
	 
}

function new_post_thumbnail_meta_box() {
	global $post;
	$thumbnail_id = get_post_meta( $post->ID, '_thumbnail_id', true );
	echo _wp_post_thumbnail_html( $thumbnail_id );
	echo ($post_type == "filial") ?  "<small>É Recomendado não ultrapassar 120px de altura!</small>" : null;
}

function render_new_post_thumbnail_meta_box() {
	global $post_type; // lets call the post type
	// remove the old meta box
	remove_meta_box( 'postimagediv','post','side' );
	// adding the new meta box.
	if ($post_type == "noticias")
	{
		add_meta_box('postimagediv', __('Imagem Destaque'), 'new_post_thumbnail_meta_box', $post_type, 'side', 'high');
	}
	elseif ($post_type == "filial")
	{
		add_meta_box('postimagediv', __('Logo da Filial'), 'new_post_thumbnail_meta_box', $post_type, 'side', 'high');
	}
	elseif ($post_type == "produtos")
	{
		add_meta_box('postimagediv', __('Imagem do Produto'), 'new_post_thumbnail_meta_box', $post_type, 'side', 'high');
	}
	
}

function custom_excerpt_more( $more ) {
	return '... <small>continua.</small>';
}

function custom_excerpt_length( $length ) {
	return 80;
}

function splocadora_register_meta_boxes( $meta_boxes ) {
	$prefix = 'spl_';

	$meta_boxes[] = array(
		'id'       => 'technical_info',
		'title'    => 'Dados Técnicos',
		'pages'    => array( 'produtos' ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				// 'name' => __( 'Dados Técnicos', 'rwmb' ),
				'desc' => __( 'Descrição/dados técnicos específicos do produto', 'rwmb' ),
				'id'   => "{$prefix}dados_tecnicos",
				'type' => 'textarea',
				'cols' => 20,
				'rows' => 8,
			),
		)
	);

	$meta_boxes[] = array(
		'id'       => 'gallery',
		'title'    => 'Galeria de Imagens',
		'pages'    => array( 'produtos' ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				// 'name'             => __( 'Galeria de Imagens', 'rwmb' ),
				'id'               => "{$prefix}plupload",
				'type'             => 'plupload_image',
				'max_file_uploads' => 6,
			),
		)
	);

	return $meta_boxes;
}


function remove_jquery_migrate( &$scripts) {
	if(!is_admin()) {
		$scripts->remove('jquery');
		$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
	}
}

function wp_custom_breadcrumbs() {
 
	$showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$delimiter   = '&nbsp;'; // delimiter between crumbs
	$home        = 'Home'; // text for the 'Home' link
	$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
	$before      = '<li class="current">'; // tag before the current crumb
	$after       = '</li>'; // tag after the current crumb
 
	global $post;
	$homeLink = get_bloginfo('url');
 
	if (is_home() || is_front_page()) {
 
		if ($showOnHome == 1) echo '<ul class="sub-nav" id="crumbs"><li><a href="' . $homeLink . '">' . $home . '</a></li></div>';
 
	} else {
 
		echo '<ul class="sub-nav" id="crumbs"><li><a href="' . $homeLink . '">' . $home . '</a></li> ' . $delimiter . ' ';
 
		if ( is_category() ) {
			$thisCat = get_category(get_query_var('cat'), false);
			if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
			echo $before . 'categoria "' . single_cat_title('', false) . '"' . $after;
 
		} elseif ( is_search() ) {
			echo $before . 'Resultado de busca para "' . get_search_query() . '"' . $after;
 
		} elseif ( is_day() ) {
			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
			echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
			echo $before . get_the_time('d') . $after;
 
		} elseif ( is_month() ) {
			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
			echo $before . get_the_time('F') . $after;
 
		} elseif ( is_year() ) {
			echo $before . get_the_time('Y') . $after;
 
		} elseif ( is_single() && !is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				echo '<li><a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a></li>';
				if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
				if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
				echo $cats;
				if ($showCurrent == 1) echo $before . get_the_title() . $after;
			}
 
		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			echo $before . $post_type->labels->singular_name . $after;
 
		} elseif ( is_attachment() ) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
			echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
			if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
		} elseif ( is_page() && !$post->post_parent ) {
			if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			for ($i = 0; $i < count($breadcrumbs); $i++) {
				echo $breadcrumbs[$i];
				if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
			}
			if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
		} elseif ( is_tag() ) {
			echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
		} elseif ( is_author() ) {
			 global $author;
			$userdata = get_userdata($author);
			echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
		} elseif ( is_404() ) {
			echo $before . 'Error 404' . $after;
		}
 
		if ( get_query_var('paged') ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
			echo __('Page') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}
 
		echo '</ul>';
 
	}
} // end wp_custom_breadcrumbs()

function check_server() {
	echo "<pre>";
	print_r($_SERVER);
	print_r( bloginfo('template_url') );
	die();
}

add_action( 'phpmailer_init', 'my_phpmailer_example' );
function my_phpmailer_example( $phpmailer ) {
	$phpmailer->isSMTP();
	$phpmailer->SMTPDebug  = 0;
	$phpmailer->Host       = 'mail.foccolocadora.com.br';
	$phpmailer->Port       = 465;
	$phpmailer->SMTPSecure = 'ssl';
	$phpmailer->SMTPAuth   = true;
	$phpmailer->Username   = 'mailer@foccolocadora.com.br';
	$phpmailer->Password   = 'La}.o@#U{$lZ';
	$phpmailer->setFrom('mailer@foccolocadora.com.br', 'Mr. Mailer');
	$phpmailer->addReplyTo('no-reply@foccolocadora.com.br', 'Não Precisa Responder');
	$phpmailer->IsHTML(true);
}

add_action( 'wp_ajax_send_message', 'do_send_message' );
add_action( 'wp_ajax_nopriv_send_message', 'do_send_message' );

function do_send_message() {

	if ( isset($_POST['data']) && !empty($_POST['data']) ) {

		try {
			$formData = $_POST['data'];
			$name     = $formData["name"];
			$email    = $formData["email"];
			define('CONTACT_EMAIL', 'locacao.focco@gmail.com');

			$message  = "<table cellpadding='16' border='0'>";
			$message .= "<tr><td><b>Empresa:</b></td><td>" . $formData["corp"] . "</td></tr>";
			$message .= "<tr><td><b>Nome de Contato:</b></td><td>" 	. $formData["name"] . "</td></tr>";
			$message .= "<tr><td><b>CNPJ</b></td><td>" . $formData["cnpj"] . "</td></tr>";
			$message .= "<tr><td><b>Endereço</b></td><td>" . $formData["address"] . "</td></tr>";
			$message .= "<tr><td><b>Número</b></td><td>" . $formData["address_number"] . "</td></tr>";
			$message .= "<tr><td>";
			$message .= "<tr><td><b>Email</b></td><td>" . $formData["email"] . "</td></tr>";
			$message .= "<tr><td><b>Telefone Fixo</b></td><td>(" . $formData["ddd"] . ") " . $formData["phone"] . "</td></tr>";
			$message .= "<tr><td><b>Observações</b></td><td><br>" . $formData["obs"] . "<br></td></tr>";
			$message .= "</table>";

			if (isset($formData["prodData"])) {
				$message .= '<br><br><br>';
				$message .= '<h2>Produtos:</h2>';
				$message .= '<table cellpadding="16" border="0"><tr><td colspan="2">';
				foreach ($formData["prodData"] as $id => $prodData) {
						$message .= '<div style="width:100px;display:inline-block;margin:12px;">';
						$message .= '	<h3 style="margin-bottom: 4px;">' . $prodData["name"] . '</h3>';
						$message .= '	<span><b>Quantidade: </b>' . $prodData["qt"] . '</span><br><br>';
						$message .= '	<center><img src="' . $prodData["thumbUrl"] . '" alt="' . $prodData["name"] . '" width="36" height="36"></center>';
						$message .= '	<center><a href="' . $prodData["link"] . '">Ver Produto</a></center>';
						$message .= '</div>';
				}
				$message .= "</td></tr></table>";
			}

			$message .= '<h2>Periodo de Locação:</h2>';
			$message .= '<span>De ' . $formData["from_date"] . ' até ' . $formData["to_date"];

			$subject = "Pedido de cotação via site por: " . $name;

			$headers[] = 'Cc: Diego Cassio <diego.cassio@dominidesign.com.br>';
			$headers[] = 'Cc: Leonardo Henrique <leonardo@dominidesign.com.br>';

			$success = wp_mail(CONTACT_EMAIL, $subject, $message, $headers);

			if ($success) {
				echo "Seu pedido de orçamento foi enviado, retornaremos em breve!";
				// return true;
			} else {
				throw new Exception("Houve um erro com o envio do seu orçamento, lamentamos! Tente outra vez, por favor!", 1);
				echo "Houve um erro com o envio do seu orçamento, lamentamos! Tente outra vez, por favor!";
				
			}

			wp_die();
			
		} catch (Exception $e) {
			echo 'ERRO: ' .$e->getMessage();
		}

	} else {
		echo "Por favor, preencha os dados corretamente.";
	}

}

// add_action('init', 'check_server');

add_filter('show_admin_bar', '__return_false');
add_filter( 'excerpt_more', 'custom_excerpt_more' );
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
add_filter( 'rwmb_meta_boxes', 'splocadora_register_meta_boxes' );
add_filter( 'wp_default_scripts', 'remove_jquery_migrate' );

add_action( 'init', 'wptp_create_post_type' );

add_action( 'init', 'theme_menu' );
add_action('add_meta_boxes', 'add_custom_meta_boxes');
add_action('save_post', 'save_custom_meta_data');
add_action('wp_enqueue_scripts', 'theme_scripts');
add_action('do_meta_boxes', 'render_new_post_thumbnail_meta_box');

add_theme_support( 'html5', array( 'search-form' ) );
add_theme_support('post-thumbnails', array('page', 'filial', 'noticias', 'produtos'));

add_image_size('cart-thumb', 36, 36, true);
add_image_size('news-main', 646, 240, true);
add_image_size('news-middle', 300, 194, true);
add_image_size('news-other', 300, 124, true);

remove_action('wp_head', 'wp_generator');