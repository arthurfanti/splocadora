<?php
	$args        = array( 'post_type' => 'produtos' );
	$posts_array = get_posts( $args );
 	get_header('internas');
	
 ?>
			<div class="row">
				<div class="small-12 columns show-for-medium-up page-header">
					<h1><?= $post_type ?></h1>
					<p class="lead">Curabitur blandit tempus porttitor.</p>
					<img src="http://placehold.it/954x152/CCCCCC/333333" alt="">
				</div>
				<div class="small-8 small-centered medium-uncentered columns">
					<section role="produtos">
						<div class="row collapse">
							<article class="small-8 small-centered medium-12 medium-uncentered column productList interna">
								<ul class="medium-block-grid-3">
								<?php while ( have_posts() ) : the_post(); ?>
									<li>
										<div class="text-center">
											<?php if (has_post_thumbnail()) : the_post_thumbnail('thumbnail'); else: ?>
												<img src="http://placehold.it/150/F0F0F0&text=Imagem Indisponível" alt="<?php the_title(); ?>">
											<?php endif ?>
											<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											<a class="button uppercase tiny radius" href="<?= get_post_permalink(); ?>">Leia Mais</a>
										</div>
									</li>
								<?php endwhile; // end of the loop. ?>
								</ul>
							</article>
						</div>
					</section>

				</div>
				<div class="medium-4 show-for-medium-up columns">
					<?php get_sidebar('primary'); ?>
				</div>
			</div>

			<?php get_template_part('partials/content', 'faq'); ?>

			<?php get_template_part('partials/content', 'about'); ?>

			<?php get_template_part('partials/content', 'subscribe'); ?>
<?php get_footer(); ?>