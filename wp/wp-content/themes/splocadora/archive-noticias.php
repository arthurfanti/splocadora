<?php
	$args        = array( 'post_type' => 'noticias' );
	$posts_array = get_posts( $args );
 	get_header('internas');
	
 ?>
			<div class="row">
				<div class="small-12 columns show-for-medium-up page-header">
					<h1><?= $post_type ?></h1>
					<p class="lead">Tudo sobre o universo de locação de máquinas e equipamentos</p>
					<!--<img src="http://placehold.it/954x152/CCCCCC/333333" alt="">-->
					<img src="<?= bloginfo('template_directory'); ?>/images/noticias-focco-locadora.jpg" alt="Notícias sobre locação de máquinas e equipamentos" />
				</div>
				<div class="small-8 small-centered medium-uncentered columns">
					<?php
						$count = 0;
						if (have_posts()) {
							while (have_posts()) : the_post();
							?>
							<?php if ($count <= 0): ?>
								<div class="small-only-text-center">
									<h4><?= $posts_array[0]->post_title; ?></h4>
									<?php if (has_post_thumbnail()) : the_post_thumbnail('news-main'); else: ?>
										<img data-interchange="[//placehold.it/320x216/CCCCCC/F47C20, (small)], [//placehold.it/646x240/CCCCCC/F47C20, (medium)]" alt="">
									<?php endif ?>
									<?php the_excerpt(); ?>
									<a class="button uppercase right radius show-for-medium-up" href="<?= get_post_permalink(); ?>">Leia Mais</a>
									<a class="button uppercase tiny radius hidden-for-medium-up" href="<?= get_post_permalink(); ?>">Leia Mais</a>
								</div>
								<ul class="small-block-grid-1 medium-block-grid-2">
							<?php elseif($count > 0 && $count <= 2): ?>
								<li class="small-only-text-center">
									<?php if (has_post_thumbnail()) : the_post_thumbnail('news-middle'); else: ?>
										<img src="http://placehold.it/300x194/EEEEEE/F47C20&text=<?php the_title(); ?>" alt="">
									<?php endif ?>
									<h4><?php the_title(); ?></h4>
									<a class="button uppercase tiny radius" href="<?= get_post_permalink(); ?>">Leia Mais</a>
								</li>
							<?php elseif($count > 2 && $count <= 4): ?>
								<li class="small-only-text-center">
									<?php if (has_post_thumbnail()) : the_post_thumbnail('news-others'); else: ?>
										<img src="http://placehold.it/300x124/f0f0f0/ccc&text=<?php the_title(); ?>" alt="">
									<?php endif ?>
									<h5><?php the_title(); ?></h5>
									<a href="<?= get_post_permalink(); ?>"><small>Leia Mais...</small></a>
								</li>
							<?php else: ?>
								<li class="small-only-text-center">
									<?php the_title(); ?>
									<a href="<?= get_post_permalink(); ?>"><small>Leia Mais...</small></a>
								</li>
							<?php endif ?>
							<?php
							$count++;
							endwhile;
							echo "</ul>";
						}
					?>
				</div>
				<div class="medium-4 show-for-medium-up columns">
					<?php get_sidebar('primary'); ?>
				</div>
			</div>

			<?php get_template_part('partials/content', 'faq'); ?>

			<?php get_template_part('partials/content', 'subscribe'); ?>
<?php get_footer(); ?>