<?php get_header('internas'); ?>
			<div class="row">
				<div class="small-12 columns show-for-medium-up page-header">
					<h2>
						<?= single_cat_title( '', false ); ?>
						<!--<p class="lead"><?php printf( __( 'Categoria: %s', 'splocadora' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></p>-->
					</h2>
					<!--<img src="http://placehold.it/954x152/CCCCCC/333333" alt="">-->
					<img src="<?= bloginfo('template_directory'); ?>/images/produtos-locacao-construcao-civil.jpg" alt="Produtos locação de máquinas e equipamentos" />
				</div>
				<div class="small-8 small-centered medium-uncentered columns">
					<section role="produtos">
						<div class="row collapse">
							<article class="small-8 small-centered medium-12 medium-uncentered column productList interna">
								<ul class="medium-block-grid-3">
								<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
									<li>
										<div class="text-center">
											<?php the_post_thumbnail('thumbnail'); ?>
											<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											<a class="button uppercase tiny radius" href="<?= get_post_permalink(); ?>">Leia Mais</a>
										</div>
									</li>
								<?php endwhile; // end of the loop. ?>
								<?php else: ?>
									<h3>nothing to see here!</h3>
								<?php endif; ?>
								</ul>
							</article>
						</div>
					</section>

				</div>
				<div class="medium-4 show-for-medium-up columns">
					<?php get_sidebar('primary'); ?>
				</div>
			</div>

			<section role="faq" class="show-for-small-only">
				<div class="row">
					<div class="small-10 small-centered columns show-for-small-only">
						<fieldset>
							<legend>dúvidas frequentes</legend>
							<dl>
								<i class="icon-question"></i>
								<dt>Etiam</dt>
								<dd>Aenean lacinia bibendum nulla sed consectetur.</dd>
							</dl>
						</fieldset>
					</div>
					<div class="small-10 small-centered medium-3 medium-uncentered columns">
						<div data-interchange="[<?= bloginfo('template_directory'); ?>/images/guia_construcao_small.jpg, (small)], [<?= bloginfo('template_directory'); ?>/images/guia_construcao.jpg, (medium)]">
							<h5 class="uppercase text-center">Guia Completo da Construção Civil</h5>
							<a href="#" class="button expand tiny uppercase">veja mais</a>
						</div>
					</div>
					<div class="small-10 small-centered medium-3 medium-uncentered columns">
						<div data-interchange="[<?= bloginfo('template_directory'); ?>/images/na_obra_small.jpg, (small)], [<?= bloginfo('template_directory'); ?>/images/na_obra.jpg, (medium)]">
							<h5 class="uppercase text-center">Na Obra...</h5>
							<a href="#" class="button expand tiny uppercase">veja mais</a>
						</div>
					</div>
				</div>
			</section>

			

			<section role="newsletter">
				<div class="row collapse">
					<div class="small-10 small-centered medium-12 medium-uncentered column">
						<h3 class="text-center uppercase">inscreva-se em nossa newsletter</h3>
						<p class="text-center">
							Ao inserir seu email você receberá automaticamente 10% de desconto na locação de uma de nossas máquinas. Além disso periodicamente receberá novidades em nosso portfólio de produtos.
						</p>
						<form action="#">
							<div class="row collapse">
								<div class="small-10 columns">
									<input type="text" placeholder="Receba nossas novidades!">
								</div>
								<div class="small-2 columns">
									<a href="#" class="button secondary large postfix">enviar</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</section>
<?php get_footer(); ?>