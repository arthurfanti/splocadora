<?php get_header('internas'); ?>
			<div class="row">
				<div class="small-12 columns page-header">
					<h1>
						<?php while ( have_posts() ) : the_post(); ?>
						<?php the_title(); ?>
						<?php endwhile; // end of the loop. ?>
					</h1>
					<p class="lead">Alugar máquinas é o nosso negócio</p>
					<img src="<?= bloginfo('template_directory'); ?>/images/focco-locadora-de-maquinas-e-equipamentos-construcao-civil.jpg" alt="Focco Locadora de máquinas e equipamentos construção civil" />
				</div>
				<div class="small-8 columns">
					<article role="postContent">
						<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
						<?php endwhile; // end of the loop. ?>
					</article>
				</div>
				<div class="small-4 columns">
					<?php get_sidebar('primary'); ?>
				</div>
			</div>
			
			<?php get_template_part('partials/loop', 'related_news'); ?>

			<?php get_template_part('partials/content', 'subscribe'); ?>
<?php get_footer(); ?>