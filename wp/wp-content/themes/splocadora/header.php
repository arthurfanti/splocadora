<!doctype html>
<!--[if IE 6]>
<html class-"no-ja" id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html class-"no-ja" id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class-"no-ja" id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html class-"no-ja" <?php language_attributes(); ?>>
<!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php
			// Print the <title> tag based on what is being viewed.
			global $page, $paged;

			wp_title( '|', true, 'right' );

			// Add the blog name.
			bloginfo( 'name' );

			// Add the blog description for the home/front page.
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
				echo " &middot; $site_description";

			// Add a page number if necessary:
			if ( $paged >= 2 || $page >= 2 )
				echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );
		?></title>

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<?php wp_head(); ?>
	</head>
	<body>
		<div data-alert class="alert-box success fixed" style="display:none;">Cadastro realizado com sucesso!<a href="#" class="close">&times;</a></div>
		<div data-alert class="alert-box warn fixed" style="display:none;">Desculpe, algo deu errado :( Tente novamente mais tarde!<a href="#" class="close">&times;</a></div>
		<header>
			<div class="topbar show-for-medium-up">
				<div class="row collapse">
					<div class="small-12 column">
						<ul class="inline-list">
							<li>
								<a href="#">guia completo da construção civil</a>
							</li>
							<li>
								<a href="#">dúvidas frequentes</a>
							</li>
							<li>
								<a href="#">locação de máquinas e equipamentos</a>
							</li>
							<li class="right">
								telefone: <span>0800 770-8300</span>
							</li>
							<li class="right">
								email: <span><a href="mailto:locacao@foccolocadora.com.br" style="color:#F47C20; font-size:1rem;">locacao@foccolocadora.com.br</a></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="mainbar">
				<div class="row collapse">
					<div class="small-3 small-centered medium-uncentered columns">
						<a href="<?= get_page_uri('home'); ?>"><b class="logo">Focco Locadora &middot; Andaimes e Construção</b></a>
					</div>

					<div class="show-for-medium-up medium-4 columns">
						<span>
							Alugar máquinas
							<em>é o nosso negócio</em>
						</span>
					</div>

					<div class="show-for-medium-up medium-5 columns">
						<div class="icon-bar four-up">
							<div class="item">
								<a href="#" class="icon-phone"></a>
								<label for="">na obra</label>
							</div>
							<!--
							<div class="item">
								<a href="<?= get_page_uri( get_page_by_title('filiais') ); ?>" class="icon-home"></a>
								<label for="">filiais</label>
							</div>
						    -->
							<div class="item">
								<a href="#" class="icon-paper"></a>
								<label for="">mídia</label>
							</div>
							<div class="item">
								<a href="<?= get_permalink( get_page_by_title('contato') ); ?>" class="icon-mail"></a>
								<label for="">contato</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="navbar show-for-medium-up">
				<div class="row collapse">
					<div class="small-12 column">
						<nav>
							<ul>
								<li><a id="menuProdutos" href="#">Produtos</a></li>
								<li><a href="http://www.foccolocadora.com.br/focco-locadora-de-maquinas-e-equipamentos/">Focco Locadora</a></li>
								<li><a href="<?= get_post_type_archive_link('noticias'); ?>">Notícias</a></li>
								<li><a href="<?= get_permalink( get_page_by_title('orcamento') );  ?>">Orçamento Rápido</a></li>
								<li><a href="<?= get_permalink( get_page_by_title('contato') ); ?>">Contato</a></li>
								<li>&nbsp;<?php get_search_form(); ?></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>

			<?php get_template_part('partials/content', 'productnav'); ?>
		</header>

		<section role="banner">
			<ul class="home-slider" data-orbit data-options="animation:fade; bullets:true; slide_number:false; timer:false;">
			<?= get_template_part('partials/loop', 'banner'); ?>
			</ul>
		</section>

		<?php get_template_part('partials/content', 'fixednav'); ?>
		<div id="content" role="main">