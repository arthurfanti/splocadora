<?php get_header('internas'); ?>
			<div class="row">
				<div class="small-12 columns page-header">
					<h2>
						<?php the_title() ?>
						<p class="lead">Curabitur blandit tempus porttitor.</p>
					</h2>
				</div>
				<div class="small-8 columns">
					<article role="postContent">
						<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
						<?php endwhile; // end of the loop. ?>
					</article>
				</div>
				<div class="small-4 columns">
					<?php get_sidebar('primary'); ?>
				</div>
			</div>
			

			<section role="related-news" class="show-for-large-only">
				<div class="row collapse">
					<div class="small-12 column">
						<ul class="large-block-grid-4">
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>

			<section role="newsletter">
				<div class="row collapse">
					<div class="small-10 small-centered medium-12 medium-uncentered column">
						<h3 class="text-center uppercase">inscreva-se em nossa newsletter</h3>
						<p class="text-center">
							Nullam id dolor id nibh ultricies vehicula ut id elit. Curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.
						</p>
						<form action="#">
							<div class="row collapse">
								<div class="small-10 columns">
									<input type="text" placeholder="Receba nossas novidades!">
								</div>
								<div class="small-2 columns">
									<a href="#" class="button secondary large postfix">enviar</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</section>
<?php get_footer(); ?>