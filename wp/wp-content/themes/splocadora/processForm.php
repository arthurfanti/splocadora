<?php
	// error_reporting(E_ALL);
	get_header('internas');

	if (isset($_GET["action"])) {
		$formData = json_decode(file_get_contents('php://input'), true);
		switch ($_GET["action"]) {
			case 'budgetForm':
				header("HTTP/1.0 200 OK");

				try {
					$name  = $formData["name"];
					$email = $formData["email"];

					define('CONTACT_EMAIL', 'locacao@foccolocadora.com.br');

					$message  = "<table cellpadding='16' border='0'>";
					$message .= "<tr><td><b>Empresa:</b></td><td>" . $formData["corp"] . "</td></tr>";
					$message .= "<tr><td><b>Nome de Contato:</b></td><td>" 	. $formData["name"] . "</td></tr>";
					$message .= "<tr><td><b>CNPJ</b></td><td>" . $formData["cnpj"] . "</td></tr>";
					$message .= "<tr><td><b>Endereço</b></td><td>" . $formData["address"] . "</td></tr>";
					$message .= "<tr><td><b>Número</b></td><td>" . $formData["address_number"] . "</td></tr>";
					$message .= "<tr><td>";
					$message .= "<tr><td><b>Email</b></td><td>" . $formData["email"] . "</td></tr>";
					$message .= "<tr><td><b>Telefone Fixo</b></td><td>(" . $formData["ddd"] . ") " . $formData["phone"] . "</td></tr>";
					$message .= "<tr><td><b>Observações</b></td><td><br>" . $formData["obs"] . "<br></td></tr>";
					$message .= "</table>";

					if (isset($formData["prodData"])) {
						$message .= '<br><br><br>';
						$message .= '<h2>Produtos:</h2>';
						$message .= '<table cellpadding="16" border="0"><tr><td colspan="2">';
						foreach ($formData["prodData"] as $id => $prodData) {
								$message .= '<div style="width:100px;display:inline-block;margin:12px;">';
								$message .= '	<h3 style="margin-bottom: 4px;">' . $prodData["name"] . '</h3>';
								$message .= '	<span><b>Quantidade: </b>' . $prodData["qt"] . '</span><br><br>';
								$message .= '	<center><img src="' . $prodData["thumbUrl"] . '" alt="' . $prodData["name"] . '" width="36" height="36"></center>';
								$message .= '	<center><a href="' . $prodData["link"] . '">Ver Produto</a></center>';
								$message .= '</div>';
						}
						$message .= "</td></tr></table>";
					}

					$message .= '<h2>Periodo de Locação:</h2>';
					$message .= '<span>De ' . $formData["from_date"] . ' até ' . $formData["to_date"];

					$headers  = 'MIME-Version: 1.0' . "\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\n";
					// $headers .= "From: \"$name\" <" . $email . ">" . "\n";
					$headers .= "Cc: leonardo@dominidesign.com.br" . "\n";
					$headers .= "Cc: arthur@dominidesign.com.br" . "\n";
					$headers .= "Reply-To: " . CONTACT_EMAIL . "\r\n";
					$headers .= "X-Mailer: PHP/" . phpversion();

					// $result = mail(CONTACT_EMAIL, "SP Locadora | Formulário de Orçamento", $message, $headers, "-f " . CONTACT_EMAIL);
					$result = wp_mail( CONTACT_EMAIL, "SP Locadora | Formulário de Orçamento", $message, $headers );

				  echo "ok";
				} catch (Exception $e) {
				  echo "error";
				}
				break;
			case 'contactForm':
				header("HTTP/1.0 200 OK");

				try {
					$name  = $formData["name"];
					$email = $formData["email"];

					define('CONTACT_EMAIL', 'locacao@foccolocadora.com.br');

					$message  = "<table cellpadding='16' border='0'>";
					$message .= "<tr><td><b>Nome de Contato:</b></td><td>" 	. $formData["name"] . "</td></tr>";
					$message .= "<tr><td><b>Email</b></td><td>" . $formData["email"] . "</td></tr>";
					$message .= "<tr><td><b>Telefone Fixo</b></td><td>(" . $formData["ddd"] . ") " . $formData["phone"] . "</td></tr>";
					$message .= "<tr><td><b>Mensagem</b></td><td><br>" . $formData["msg"] . "<br></td></tr>";
					$message .= "</table>";

					$headers  = 'MIME-Version: 1.0' . "\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\n";
					$headers .= "From: \"$name\" <" . $email . ">"  . "\n";
					$headers .= "Cc: leonardo@dominidesign.com.br" . "\n";
					$headers .= "Reply-To: " . CONTACT_EMAIL . "\r\n";
					$headers .= "X-Mailer: PHP/" . phpversion();

					$result = mail(CONTACT_EMAIL, "SP Locadora | Formulário de Contato", $message, $headers, "-f " . CONTACT_EMAIL);

				  echo "ok";
				} catch (Exception $e) {
				  echo "error";
				}
				break;
			default:
				echo  "Ação não reconhecida.";
				break;
		}
	}
get_footer();
