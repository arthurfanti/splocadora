<?php get_header('internas'); ?>
			<div class="row">
				<div class="small-12 columns page-header">
					<h1>
						<!--<?php the_title() ?>-->
						Contato
					</h1>
					<p class="lead">Alugar máquinas é nosso trabalho</p>
				</div>
				<div class="small-8 columns">
					<article role="budgetContent">
						<form name="senddata" method="POST">
							<fieldset>
								<legend>Dados de contato</legend>
								<div class="row">
									<div class="small-12 columns">
										<label for="name">Nome para contato:</label>
										<input type="text" id="name" name="name" placeholder="Nome" tabindex="2">
									</div>
									<div class="small-12 columns">
										<label for="email">Email:</label>
										<input type="text" id="email" name="email" placeholder="Email" tabindex="3">
									</div>
									<div class="small-2 columns">
										<label for="ddd">DDD:</label>
										<input type="text" id="ddd" name="ddd" placeholder="DDD" maxlength="2" tabindex="5">
									</div>
									<div class="small-10 columns">
										<label for="phone">Telefone:</label>
										<input type="text" id="phone" name="phone" placeholder="Telefone" tabindex="6">
									</div>
									<div class="small-12 columns">
										<label for="phone">Assunto:</label>
										<select name="subject" id="subject" tabindex="7">
											<option value="duvidas">Dúvidas</option>
											<option value="reclamacoes">Reclamações</option>
											<option value="pedidos">Pedidos</option>
											<option value="sugestoes">Sugestões</option>
										</select>
									</div>
									<div class="small-12 columns">
										<label for="msg">Mensagem:</label>
										<textarea name="msg" id="msg" cols="30" rows="16" tabindex="8"></textarea>
									</div>
								</div>
							</fieldset>
							<button type="submit" class="tiny radius secondary uppercase right" tabindex-"9">enviar</button>
						</form>
					</article>
				</div>
				<div class="small-4 columns">
					<?php get_sidebar('primary'); ?>
				</div>
			</div>
			

			<section role="related-news" class="show-for-large-only">
				<div class="row collapse">
					<div class="small-12 column">
						<ul class="large-block-grid-4">
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>

			<section role="newsletter">
				<div class="row collapse">
					<div class="small-10 small-centered medium-12 medium-uncentered column">
						<h3 class="text-center uppercase">inscreva-se em nossa newsletter</h3>
						<p class="text-center">
							Ao inserir seu email você receberá automaticamente 10% de desconto na locação de uma de nossas máquinas. Além disso periodicamente receberá novidades em nosso portfólio de produtos.
						</p>
						<form action="#">
							<div class="row collapse">
								<div class="small-10 columns">
									<input type="text" placeholder="Receba nossas novidades!">
								</div>
								<div class="small-2 columns">
									<a href="#" class="button secondary large postfix">enviar</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</section>
<?php get_footer(); ?>