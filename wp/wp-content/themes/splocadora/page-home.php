<?php get_header(); ?>
			<section role="news">
				<div class="row">
					<?php
						$latest = new WP_Query(array('post_type' => 'noticias', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date'));
                    	if ($latest->have_posts()) : while ($latest->have_posts()) : $latest->the_post();
                    ?>
					<div class="small-12 medium-5 columns text-center">
						<?php if (has_post_thumbnail()) :  the_post_thumbnail(400,400); else :?>
							<img data-interchange="[//lorempixel.com/380/120/transport, (default)], [//lorempixel.com/400/400/FFFFFF&text=<?php the_title(); ?>, (medium)]" >
						<?php endif ?>
					</div>
					<div class="small-10 small-centered medium-7 medium-uncentered columns">
						<span class="date"><?php the_time("d"); ?>.<?php the_time("m"); ?>.<?php the_time("Y"); ?></span>
						<p class="title"><?php the_title(); ?></p>
						<p class="text-justify">
							<?php echo substr(get_the_excerpt(), 0,875); ?>
						</p>
						<a href="<?= get_post_permalink(); ?>" class="button tiny radius">LEIA MAIS</a>
					</div>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<div class="small-10 small-centered column">
							<h2 class="n_encontrado">Não encontrado!</h2>
							<p class="n_encontrado">Desculpe, mas não encontramos o que você procura.</p>
						</div>
					<?php endif; ?>
					<!-- <div class="show-for-medium-up medium-3 columns">
						<div class="twitterWrapper icon-twitter">
							Focco Locadora sempre trazendo novidades e tecnologia de ponta para a Construção Civil <a href="#">pic.tiwitter.com/SPLOCADORA</a>
						</div>

						<div class="twitterFollow">
							<a href="https://twitter.com/splocadora" class="twitter-follow-button" data-show-count="false" data-lang="pt" data-dnt="true">Seguir @splocadora</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						</div>
					</div> -->
				</div>
			</section>

			<?php get_template_part('partials/loop', 'related_news'); ?>

			<section role="produtos">
				<div class="row collapse">
					<div class="small-12 column">
						<h3 class="icon-search text-center primary uppercase">
							confira nossos produtos
							<p>A Focco Locadora é líder de mercado em locação de máquinas e equipamentos para construção civil. Alugar máquinas está no DNA da empresa através de equipamentos de última geração. Confira abaixo nosso acervo:</p>
						</h3>
					</div>

					<article class="small-8 small-centered medium-12 medium-uncentered column productList home">
<<<<<<< HEAD
						<ul class="large-block-grid-4">
							<?php $products_query = new WP_Query(array('post_type' => 'produtos', 'posts_per_page' => 8, 'order' => 'DESC', 'orderby' => 'date')); ?>
							<?php if ( $products_query->have_posts() ) : while ( $products_query->have_posts() ) : $products_query->the_post(); ?>
							<?php
								if (has_post_thumbnail()) {
									$_prod_thumbs = wp_get_attachment_image_src(get_post_thumbnail_id(), array(36,36));
									$_prod_thumb  = $_prod_thumbs[0];
								} else {
									$_prod_thumb = "";
								}
							?>
							<!-- post -->
							<li>
								<div class="text-center">
									<?= the_post_thumbnail( array(240,240) ); ?>
									<h4 class="primary"><?= the_title(); ?></h4>
									<p>
										<?= substr(get_the_excerpt(), 0, 97) . '...' ?>
									</p>
									<a href="<?= the_permalink(); ?>" class="button uppercase tiny radius">ver mais</a>
									<a href="#" class="button secondary uppercase tiny radius add-to-cart-button" data-prod-info='{"id":"<?php the_ID(); ?>","name":"<?php the_title(); ?>","link":"<?php the_permalink() ?>","thumbUrl":"<?php echo $_prod_thumb; ?>"}'>adicionar&nbsp;<i class="icon-cart"></i>
									</a>
=======
						<ul class="large-block-grid-4">	
						<?php $products_query = new WP_Query(array('post_type' => 'produtos', 'posts_per_page' => 8, 'order' => 'DESC', 'orderby' => 'date')); ?>
						<?php if ( $products_query->have_posts() ) : while ( $products_query->have_posts() ) : $products_query->the_post(); ?>
						<?php
						if (has_post_thumbnail()) {
						$_prod_thumbs = wp_get_attachment_image_src(get_post_thumbnail_id(), array(36,36));
						$_prod_thumb  = $_prod_thumbs[0];
						} else {
						$_prod_thumb = "";
						}
						?>
						<!-- post -->
							<li>
								<div class="text-center">
									<?php if (has_post_thumbnail()) : the_post_thumbnail('news-middle'); else: ?>
										<img src="//placehold.it/330x268/F0F0F0&text=Imagem Indisponível" alt="<?php the_title(); ?>">
									<?php endif ?>
									<h4 class="primary"><?php the_title(); ?></h4>
									<p><?= substr(get_the_excerpt(), 0, 97); ?>...</p>
									<a href="<?= the_permalink(); ?>" class="button uppercase tiny radius">ver mais</a>
									<a href="#" class="button secondary uppercase tiny radius add-to-cart-button" data-prod-info='{"id":"<?php the_ID(); ?>","name":"<?php the_title(); ?>","link":"<?php the_permalink() ?>","thumbUrl":"<?php echo $_prod_thumb; ?>"}'>adicionar&nbsp;<i class="icon-cart"></i></a>
>>>>>>> mudancas_via_ftp
								</div>
							</li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
							<!-- post navigation -->
							<?php else: ?>
							<h4>:(</h4>
							<?php endif; ?>
						</ul>
					</article>

					<div class="small-12 column text-center">
						<a href="<?= get_post_type_archive_link('produtos'); ?>" class="btn-prods">acessar todos produtos</a>
					</div>
				</div>
			</section>

			<?php get_template_part('partials/content', 'faq'); ?>

			<?php get_template_part('partials/content', 'about'); ?>

			<section role="dados" class="show-for-medium-up">
				<div class="row">
					<div class="small-12 column">
						<h3 class="text-center uppercase">
							porque somos líderes como locadores de máquinas
						</h3>
						<p class="text-center">
							Agilidade e rapidez na entrega das máquinas e equipamentos alugados é um dos pontos altos dos serviços da Focco Locadora. Os produtos irão chegar sempre com segurança no destino final, o que pode economizar tempo e dinheiro para o locatário.
						</p>
					</div>
					<div class="small-3 columns">
						<h4 class="uppercase">Andaimes e escoramentos</h4>
						<ul>
							<li><a href="http://www.foccolocadora.com.br/produto/barra-de-ancoragem/" target="_self" title="#">locação de barra de ancoragem</a></li>
							<li><a href="http://www.foccolocadora.com.br/produto/andaime-tubular-2/" target="_self" title="#">locação de andaime tubular</a></li>
							<li><a href="http://www.foccolocadora.com.br/produto/andaime-fachadeiro-2/" target="_self" title="#">locação de andaime fachadeiro</a></li>
						</ul>
					</div>
					<div class="small-4 columns">
						<h4 class="uppercase">Compressores e Ferramentas Pneumáticas</h4>
						<ul>
							<li><a href="http://www.foccolocadora.com.br/produto/rompedor-pneumatico/" target="_self" title="#">locação de rompedor pneumático</a></li>
							<li><a href="http://www.foccolocadora.com.br/produto/dobradeira-de-tubo/" target="_self" title="#">locação de dobradeira de tubo</a></li>
							<li><a href="http://www.foccolocadora.com.br/produto/alicate-prensa-terminal-2/" target="_self" title="#">locação de alicate prensa terminal</a></li>
						</ul>
					</div>
					<div class="small-2 columns">
						<h4 class="uppercase">Concretagem, alvenaria e acabamento</h4>
						<ul>
							<li><a href="http://www.foccolocadora.com.br/produto/carro-girica/" target="_self" title="#">locação de carro girica</a></li>
							<li><a href="http://www.foccolocadora.com.br/produto/serra-bancada/" target="_self" title="#">locação de serra bancada</a></li>
							<li><a href="http://www.foccolocadora.com.br/produto/fresadora-de-piso/" target="_self" title="#">locação de fresadora de piso</a></li>
						</ul>
					</div>
					<div class="small-3 columns">
						<h4 class="uppercase">Equipamentos Elevação de materiais</h4>
						<ul>
							<li><a href="http://www.foccolocadora.com.br/produto/mini-grua/" target="_self" title="#">locação de mini grua</a></li>
							<li><a href="http://www.foccolocadora.com.br/produto/tirfor/" target="_self" title="#">locação de tirfor</a></li>
							<li><a href="http://www.foccolocadora.com.br/produto/talha-catraca/" target="_self" title="#">locação de talha catraca</a></li>
						</ul>
					</div>
				</div>
			</section>

			<?php get_template_part('partials/content', 'subscribe'); ?>
<?php get_footer(); ?>