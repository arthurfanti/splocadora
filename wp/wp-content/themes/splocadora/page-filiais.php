<?php get_header('internas'); ?>
			<div class="row">
				<div class="small-12 columns page-header">
					<h2>
						<?php the_title() ?>
						<p class="lead">Curabitur blandit tempus porttitor.</p>
					</h2>
					<img src="http://placehold.it/954x152/CCCCCC/333333" alt="">
				</div>
				<div class="small-8 small-centered medium-uncentered columns">
					<ul class="small-block-grid-1 medium-block-grid-2">
						<?php get_template_part('partials/loop', 'filiais'); ?>
					</ul>

					<div id="map_canvas" style="height:420px; margin-bottom:1rem;"></div>
				</div>
				<div class="medium-4 show-for-medium-up columns">
					<?php get_sidebar('primary'); ?>
				</div>
			</div>
			

			<section role="related-news" class="show-for-medium-up">
				<div class="row collapse">
					<div class="small-12 column">
						<ul class="large-block-grid-4">
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
							<li>
								<img src="//placehold.it/330x268.png" alt="">
								<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.</p>
								<a href="#" class="button uppercase tiny radius">
									leia mais
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>

			<section role="faq" class="show-for-small-only">
				<div class="row">
					<div class="small-10 small-centered columns show-for-small-only">
						<fieldset>
							<legend>dúvidas frequentes</legend>
							<dl>
								<i class="icon-question"></i>
								<dt>Etiam</dt>
								<dd>Aenean lacinia bibendum nulla sed consectetur.</dd>
							</dl>
						</fieldset>
					</div>
					<div class="small-10 small-centered medium-3 medium-uncentered columns">
						<div data-interchange="[<?= bloginfo('template_directory'); ?>/images/guia_construcao_small.jpg, (small)], [<?= bloginfo('template_directory'); ?>/images/guia_construcao.jpg, (medium)]">
							<h5 class="uppercase text-center">Guia Completo da Construção Civil</h5>
							<a href="#" class="button expand tiny uppercase">veja mais</a>
						</div>
					</div>
					<div class="small-10 small-centered medium-3 medium-uncentered columns">
						<div data-interchange="[<?= bloginfo('template_directory'); ?>/images/na_obra_small.jpg, (small)], [<?= bloginfo('template_directory'); ?>/images/na_obra.jpg, (medium)]">
							<h5 class="uppercase text-center">Na Obra...</h5>
							<a href="#" class="button expand tiny uppercase">veja mais</a>
						</div>
					</div>
				</div>
			</section>

			<section role="historia">
				<div class="row">
					<div class="small-12 columns">
						<h3 class="text-center">
							<span class="primary">SÃO PAULO LOCADORA -</span>
							25 ANOS DE HISTÓRIA NO MERCADO DE LOCADORES DE MÁQUINAS E EQUIPAMENTOS
						</h3>
					</div>
					<div class="small-12 medium-3 columns">
						<img data-interchange="[//placehold.it/350x100/212121/F47C20.png&text=empresa.png, (default)], [//placehold.it/275x202/333/F47C20.png&text=empresa.png, (medium)]" alt="">
						<noscript><img src="//placehold.it/275x202.png" alt=""></noscript>
					</div>
					<div class="small-12 small-only-text-justify medium-9 columns">
						<p>
							Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Sed posuere consectetur est at lobortis. Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							<br>
							Maecenas faucibus mollis interdum. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
						</p>
						<div class="text-center">
							<a href="#" class="button radius uppercase">conheça nossa história</a>
						</div>
					</div>
				</div>
			</section>

			<section role="newsletter">
				<div class="row collapse">
					<div class="small-10 small-centered medium-12 medium-uncentered column">
						<h3 class="text-center uppercase">inscreva-se em nossa newsletter</h3>
						<p class="text-center">
							Nullam id dolor id nibh ultricies vehicula ut id elit. Curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.
						</p>
						<form action="#">
							<div class="row collapse">
								<div class="small-10 columns">
									<input type="text" placeholder="Receba nossas novidades!">
								</div>
								<div class="small-2 columns">
									<a href="#" class="button secondary large postfix">enviar</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</section>
<?php get_footer(); ?>